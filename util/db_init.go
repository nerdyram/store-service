package util

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"store-service/model"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// DBConfig holds the application configuration
type DBConfig struct {
	DbHost     string `json:"dbHost"`
	DbUsername string `json:"dbUsername"`
	DbPassword string `json:"dbPassword"`
	DbName     string `json:"dbName"`
}

var dbConfig *DBConfig

// InitDatabase is responsible for initializing the db
func InitDatabase() {
	if dbConfig == nil {
		dbConfig = readDBConfiguration()
	}

	connString := url.URL{
		User:     url.UserPassword(dbConfig.DbUsername, dbConfig.DbPassword),
		Scheme:   "postgres",
		Host:     fmt.Sprintf("%s:%d", dbConfig.DbHost, 5432),
		Path:     dbConfig.DbName,
		RawQuery: (&url.Values{"sslmode": []string{"disable"}}).Encode(),
	}

	db, err := gorm.Open("postgres", connString.String())
	if err != nil {
		log.Fatalln("failed to connect database: " + err.Error())
		os.Exit(503)
	}

	defer db.Close()

	// Create tables for models
	db.AutoMigrate(&model.Item{})
	db.AutoMigrate(&model.Order{})
	db.AutoMigrate(&model.OrderLine{})
	db.AutoMigrate(&model.Unit{})
	db.AutoMigrate(&model.Account{})
	db.AutoMigrate(&model.Session{})

	var foundAccount model.Account
	err = db.Where("username = ?", "admin").First(&foundAccount).Error
	if err != nil {
		hash, err := bcrypt.GenerateFromPassword([]byte("Regis87."), bcrypt.DefaultCost)
		if err != nil {
			os.Exit(1000)
		}
		foundAccount = model.Account{
			Username:            "admin",
			Password:            string(hash),
			PersonOrCompanyName: "Malzeme Siparişleri",
			Address:             "",
			PhoneNumber:         "",
			Role:                "ADMIN",
			Status:              "Aktif",
		}
		db.Create(&foundAccount)
	}

	var foundItem model.Item
	err = db.Where("name = ?", "Mendil").First(&foundItem).Error
	if err != nil {
		foundItem = model.Item{
			Name:           "Mendil",
			RequiresRecipe: false,
			Status:         "Aktif",
		}
		db.Create(&foundItem)
	}

}

// GetDatabase is
func GetDatabase() *gorm.DB {
	if dbConfig == nil {
		dbConfig = readDBConfiguration()
	}

	connString := url.URL{
		User:     url.UserPassword(dbConfig.DbUsername, dbConfig.DbPassword),
		Scheme:   "postgres",
		Host:     fmt.Sprintf("%s:%d", dbConfig.DbHost, 5432),
		Path:     dbConfig.DbName,
		RawQuery: (&url.Values{"sslmode": []string{"disable"}}).Encode(),
	}

	db, err := gorm.Open("postgres", connString.String())
	if err != nil {
		log.Fatalln("failed to connect database: " + err.Error())
	}

	return db
}

func readDBConfiguration() *DBConfig {
	data, readConfigErr := ioutil.ReadFile("dbconfig.json")
	if readConfigErr != nil {
		log.Fatalln("Database Configuration file is not available!")
		os.Exit(504)
	}

	dbConfig := DBConfig{}

	readConfigErr = json.Unmarshal(data, &dbConfig)

	if readConfigErr != nil {
		log.Fatalln("Database Configuration file is invalid!")
		os.Exit(505)
	}

	return &dbConfig
}
