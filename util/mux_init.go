package util

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

// Response is
type Response struct {
	Version string      `json:"version"`
	Success bool        `json:"success"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// MuxRoute is
type MuxRoute struct {
	Route      string
	Controller func(http.ResponseWriter, *http.Request)
	Method     string
}

// InitMux is
func InitMux(routes map[string]MuxRoute) http.Handler {
	router := mux.NewRouter()

	for key, val := range routes {
		router.HandleFunc(key, val.Controller).Methods(val.Method)
	}

	handler := cors.Default().Handler(router)
	return handler
}

// CreateResponse is
func CreateResponse(version string, success bool, message string, data interface{}) Response {
	return Response{
		Version: version,
		Success: success,
		Message: message,
		Data:    data,
	}
}
