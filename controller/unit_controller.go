package controller

import (
	"encoding/json"
	"net/http"
	"store-service/service"
	"store-service/util"

	"github.com/gorilla/mux"
)

// UnitHome is
func UnitHome(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"Store API - Unit Service",
		nil,
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// GetAllUnits is
func GetAllUnits(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.ListUnits(),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// GetUnit is
func GetUnit(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.GetUnit(id),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// CreateUnit is
func CreateUnit(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.CreateUnit(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// UpdateUnit is
func UpdateUnit(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.UpdateUnit(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// InitUnitServiceRoutes is responsible creating routes of SentenceService
func InitUnitServiceRoutes() map[string]util.MuxRoute {
	routes := make(map[string]util.MuxRoute)

	route := util.MuxRoute{
		Route:      "/unit",
		Controller: UnitHome,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/unit/list",
		Controller: GetAllUnits,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/unit/get/{id}",
		Controller: GetUnit,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/unit/create",
		Controller: CreateUnit,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/unit/update",
		Controller: UpdateUnit,
		Method:     "POST",
	}
	routes[route.Route] = route

	return routes
}
