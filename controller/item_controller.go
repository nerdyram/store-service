package controller

import (
	"encoding/json"
	"net/http"
	"store-service/service"
	"store-service/util"

	"github.com/gorilla/mux"
)

// ItemHome is
func ItemHome(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"Store API - Item Service",
		nil,
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// GetAllItems is
func GetAllItems(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.ListItems(),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// GetItem is
func GetItem(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.GetItem(id),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// CreateItem is
func CreateItem(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.CreateItem(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// UpdateItem is
func UpdateItem(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.UpdateItem(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// SearchItem is
func SearchItem(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.SearchItem(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// AddUnitToItem is
func AddUnitToItem(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.AddUnit(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// RemoveUnitFromItem is
func RemoveUnitFromItem(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.RemoveUnit(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// InitItemServiceRoutes is responsible creating routes of SentenceService
func InitItemServiceRoutes() map[string]util.MuxRoute {
	routes := make(map[string]util.MuxRoute)

	route := util.MuxRoute{
		Route:      "/item",
		Controller: ItemHome,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/item/list",
		Controller: GetAllItems,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/item/get/{id}",
		Controller: GetItem,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/item/create",
		Controller: CreateItem,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/item/update",
		Controller: UpdateItem,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/item/search",
		Controller: SearchItem,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/item/addUnitToItem",
		Controller: AddUnitToItem,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/item/removeUnitFromItem",
		Controller: RemoveUnitFromItem,
		Method:     "POST",
	}
	routes[route.Route] = route

	return routes
}
