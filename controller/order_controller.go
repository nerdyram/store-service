package controller

import (
	"encoding/json"
	"net/http"
	"store-service/service"
	"store-service/util"

	"github.com/gorilla/mux"
)

// OrderHome is
func OrderHome(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"Store API - Order Service",
		nil,
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// GetAllOrders is
func GetAllOrders(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.ListOrders(),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// GetOrder is
func GetOrder(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.GetOrder(id),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// CreateOrder is
func CreateOrder(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.CreateOrder(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// UpdateOrder is
func UpdateOrder(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.UpdateOrder(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// GetLastOrderAtCart is
func GetLastOrderAtCart(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.GetLastOrderAtCart(),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// InitOrderServiceRoutes is responsible creating routes of Order Service
func InitOrderServiceRoutes() map[string]util.MuxRoute {
	routes := make(map[string]util.MuxRoute)

	route := util.MuxRoute{
		Route:      "/order",
		Controller: OrderHome,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/order/list",
		Controller: GetAllOrders,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/order/get/{id}",
		Controller: GetOrder,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/order/create",
		Controller: CreateOrder,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/order/update",
		Controller: UpdateOrder,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      "/order/getLastOrderAtCart",
		Controller: GetLastOrderAtCart,
		Method:     "GET",
	}
	routes[route.Route] = route

	return routes
}
