package controller

import (
	"encoding/json"
	"net/http"
	"store-service/service"
	"store-service/util"

	"github.com/gorilla/mux"
)

// AccountHome is
func AccountHome(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"Store API - Account Service",
		nil,
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// GetAllAccounts is
func GetAllAccounts(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.ListAccounts(),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// GetAccount is
func GetAccount(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.GetAccount(id),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// CreateAccount is
func CreateAccount(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.CreateAccount(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// UpdateAccount is
func UpdateAccount(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.UpdateAccount(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// Login is
func Login(w http.ResponseWriter, r *http.Request) {
	account := service.Login(r)
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		account,
	)
	if account == nil {
		response = util.CreateResponse(
			"v1.0.0",
			false,
			"USERNAME_PASSWORD_ERROR",
			nil,
		)
		w.WriteHeader(http.StatusBadRequest)
	}
	
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// Logout is
func Logout(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.Logout(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// CheckToken is
func CheckToken(w http.ResponseWriter, r *http.Request) {
	response := util.CreateResponse(
		"v1.0.0",
		true,
		"",
		service.CheckToken(r),
	)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&response)
}

// InitAccountServiceRoutes is responsible creating routes of AccountService
func InitAccountServiceRoutes() map[string]util.MuxRoute {
	const apiPrefix string = "/account"
	routes := make(map[string]util.MuxRoute)

	route := util.MuxRoute{
		Route:      apiPrefix,
		Controller: AccountHome,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      apiPrefix + "/list",
		Controller: GetAllAccounts,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      apiPrefix + "/get/{id}",
		Controller: GetAccount,
		Method:     "GET",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      apiPrefix + "/create",
		Controller: CreateAccount,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      apiPrefix + "/update",
		Controller: UpdateAccount,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      apiPrefix + "/login",
		Controller: Login,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      apiPrefix + "/logout",
		Controller: Logout,
		Method:     "POST",
	}
	routes[route.Route] = route

	route = util.MuxRoute{
		Route:      apiPrefix + "/checkToken",
		Controller: CheckToken,
		Method:     "POST",
	}
	routes[route.Route] = route

	return routes
}
