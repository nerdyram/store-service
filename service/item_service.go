package service

import (
	"encoding/json"
	"net/http"
	"store-service/model"
	"store-service/util"
)

// ListItems is
func ListItems() []model.Item {
	var items []model.Item
	db := util.GetDatabase()
	db.Preload("Units").Order("name").Find(&items)
	defer db.Close()
	return items
}

// GetItem is
func GetItem(id string) *model.Item {
	var item model.Item
	db := util.GetDatabase()
	db.First(&item, id)
	defer db.Close()
	return &item
}

// CreateItem is
func CreateItem(r *http.Request) *model.Item {
	var item model.Item
	json.NewDecoder(r.Body).Decode(&item)
	db := util.GetDatabase()
	db.Create(&item)
	defer db.Close()
	return &item
}

// UpdateItem is
func UpdateItem(r *http.Request) *model.Item {
	var updatedItem model.Item
	json.NewDecoder(r.Body).Decode(&updatedItem)
	db := util.GetDatabase()
	db.Save(&updatedItem)
	defer db.Close()
	return &updatedItem
}

// SearchItemRequest is
type SearchItemRequest struct {
	Search string `json:"search"`
}

// SearchItem is
func SearchItem(r *http.Request) []model.Item {
	var searchItemRequest SearchItemRequest
	var items []model.Item
	json.NewDecoder(r.Body).Decode(&searchItemRequest)
	db := util.GetDatabase()
	search := searchItemRequest.Search + "%"
	db.Preload("Units").Where("name like ?", search).Find(&items)
	defer db.Close()
	return items
}

// ItemUnitRequest is
type ItemUnitRequest struct {
	ItemID uint `json:"itemId"`
	UnitID uint `json:"unitId"`
}

// AddUnit is
func AddUnit(r *http.Request) *model.Item {
	var itemUnitRequest ItemUnitRequest
	var updatedItem model.Item
	var unitToAdd model.Unit
	json.NewDecoder(r.Body).Decode(&itemUnitRequest)
	db := util.GetDatabase()
	db.First(&updatedItem, itemUnitRequest.ItemID)
	db.First(&unitToAdd, itemUnitRequest.UnitID)
	db.Model(&updatedItem).Association("Units").Append(&unitToAdd)
	db.Save(&updatedItem)
	defer db.Close()
	return &updatedItem
}

// RemoveUnit is
func RemoveUnit(r *http.Request) *model.Item {
	var itemUnitRequest ItemUnitRequest
	var updatedItem model.Item
	var unitToRemove model.Unit
	json.NewDecoder(r.Body).Decode(&itemUnitRequest)
	db := util.GetDatabase()
	db.First(&updatedItem, itemUnitRequest.ItemID)
	db.First(&unitToRemove, itemUnitRequest.UnitID)
	db.Model(&updatedItem).Association("Units").Delete(&unitToRemove)
	db.Save(&updatedItem)
	defer db.Close()
	return &updatedItem
}
