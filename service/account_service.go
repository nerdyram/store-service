package service

import (
	"encoding/json"
	"net/http"
	"store-service/model"
	"store-service/util"
	"time"

	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

// ListAccounts is
func ListAccounts() []model.Account {
	var accounts []model.Account
	db := util.GetDatabase()
	db.Preload("Units").Order("name").Find(&accounts)
	defer db.Close()
	return accounts
}

// GetAccount is
func GetAccount(id string) *model.Account {
	var account model.Account
	db := util.GetDatabase()
	db.First(&account, id)
	defer db.Close()
	return &account
}

// CreateAccount is
func CreateAccount(r *http.Request) *model.Account {
	var account model.Account
	json.NewDecoder(r.Body).Decode(&account)
	hash, err := bcrypt.GenerateFromPassword([]byte(account.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil
	}
	account.Password = string(hash)
	db := util.GetDatabase()
	db.Create(&account)
	defer db.Close()
	return &account
}

// UpdateAccount is
func UpdateAccount(r *http.Request) *model.Account {
	var updatedAccount model.Account
	json.NewDecoder(r.Body).Decode(&updatedAccount)
	hash, err := bcrypt.GenerateFromPassword([]byte(updatedAccount.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil
	}
	updatedAccount.Password = string(hash)
	db := util.GetDatabase()
	db.Save(&updatedAccount)
	defer db.Close()
	return &updatedAccount
}

// LoginRequest is
type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Login is
func Login(r *http.Request) *model.Account {
	var loginRequest LoginRequest
	json.NewDecoder(r.Body).Decode(&loginRequest)
	var foundAccount model.Account
	db := util.GetDatabase()
	err := db.Preload("Session").Where("username = ?", loginRequest.Username).First(&foundAccount).Error
	if err != nil {
		return nil
	}
	passwordAsBytes := []byte(loginRequest.Password)
	err = bcrypt.CompareHashAndPassword([]byte(foundAccount.Password), passwordAsBytes)
	if err != nil {
		return nil
	}
	var session model.Session
	err = db.Model(&foundAccount).Related(&session).Error
	if err != nil {
		token := uuid.NewV4()
		session = model.Session{
			Token:     token,
			CreatedAt: time.Now(),
			Status:    "Authorized",
		}
		foundAccount.Session = session
	} else {
		foundAccount.Session.Token = uuid.NewV4()
		foundAccount.Session.CreatedAt = time.Now()
		foundAccount.Session.Status = "Authorized"
	}
	defer db.Close()
	db.Save(&foundAccount)
	return &foundAccount
}

// LogoutRequest is
type LogoutRequest struct {
	Username string `json:"username"`
}

// Logout is
func Logout(r *http.Request) *model.Account {
	var logoutRequest LogoutRequest
	json.NewDecoder(r.Body).Decode(&logoutRequest)
	var foundAccount model.Account
	db := util.GetDatabase()
	err := db.Preload("Session").Where("username = ?", logoutRequest.Username).First(&foundAccount).Error
	if err != nil {
		return nil
	}

	var session model.Session
	err = db.Model(&foundAccount).Related(&session).Error
	if err != nil {
		return nil
	}
	foundAccount.Session.Status = "Expired"
	db.Save(&foundAccount)
	defer db.Close()
	return &foundAccount
}

// CheckTokenRequest is
type CheckTokenRequest struct {
	Username string    `json:"username"`
	Token    uuid.UUID `json:"token"`
}

// CheckToken is
func CheckToken(r *http.Request) bool {
	var checkTokenRequest CheckTokenRequest
	json.NewDecoder(r.Body).Decode(&checkTokenRequest)
	var foundAccount model.Account
	db := util.GetDatabase()
	err := db.Preload("Session").Where("username = ?", checkTokenRequest.Username).First(&foundAccount).Error
	if err != nil {
		return false
	}

	var session model.Session
	err = db.Model(&foundAccount).Related(&session).Error
	if err != nil {
		return false
	}
	defer db.Close()
	return foundAccount.Session.Token == checkTokenRequest.Token
}
