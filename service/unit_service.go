package service

import (
	"encoding/json"
	"net/http"
	"store-service/model"
	"store-service/util"
)

// ListUnits is
func ListUnits() []model.Unit {
	var units []model.Unit
	db := util.GetDatabase()
	db.Order("name").Find(&units)
	defer db.Close()
	return units
}

// GetUnit is
func GetUnit(id string) *model.Unit {
	var unit model.Unit
	db := util.GetDatabase()
	db.First(&unit, id)
	defer db.Close()
	return &unit
}

// CreateUnit is
func CreateUnit(r *http.Request) *model.Unit {
	var unit model.Unit
	json.NewDecoder(r.Body).Decode(&unit)
	db := util.GetDatabase()
	db.Create(&unit)
	defer db.Close()
	return &unit
}

// UpdateUnit is
func UpdateUnit(r *http.Request) *model.Unit {
	var updatedUnit model.Unit
	json.NewDecoder(r.Body).Decode(&updatedUnit)
	db := util.GetDatabase()
	db.Save(&updatedUnit)
	defer db.Close()
	return &updatedUnit
}
