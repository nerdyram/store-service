package service

import (
	"encoding/json"
	"net/http"
	"store-service/model"
	"store-service/util"
)

// ListOrders is
func ListOrders() []model.Order {
	var orders []model.Order
	db := util.GetDatabase()
	db.Preload("OrderLines").Order("name").Find(&orders)
	defer db.Close()
	return orders
}

// GetOrder is
func GetOrder(id string) *model.Order {
	var order model.Order
	db := util.GetDatabase()
	db.First(&order, id)
	defer db.Close()
	return &order
}

// CreateOrder is
func CreateOrder(r *http.Request) *model.Order {
	var order model.Order
	json.NewDecoder(r.Body).Decode(&order)
	db := util.GetDatabase()
	db.Create(&order)
	defer db.Close()
	return &order
}

// UpdateOrder is
func UpdateOrder(r *http.Request) *model.Order {
	var updatedOrder model.Order
	json.NewDecoder(r.Body).Decode(&updatedOrder)
	db := util.GetDatabase()
	db.Save(&updatedOrder)
	defer db.Close()
	return &updatedOrder
}

// GetLastOrderAtCart is
func GetLastOrderAtCart() *model.Order {
	var order model.Order
	db := util.GetDatabase()
	err := db.Preload("Customer").Preload("OrderLines").Preload("OrderLines.Item").Preload("OrderLines.Item.Units").Preload("OrderLines.Unit").Order("id desc").Where("status = ?", "AT_CART").First(&order).Error
	defer db.Close()
	if err != nil {
		return &model.Order{
			ID:     0,
			Status: "NON_EXISTENT",
		}
	}
	return &order
}
