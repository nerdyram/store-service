package model

// Order will hold information about an order.
type Order struct {
	ID         uint        `gorm:"primary_key" json:"id"`
	OrderLines []OrderLine `gorm:"foreignkey:OrderID" json:"orderLines"`
	Status     string      `json:"status"` // AT_CART, NEW, APPROVED, FINALIZED
	CustomerID uint        `json:"-"`
	Customer   Account     `gorm:"association_autoupdate:false;association_autocreate:false;foreignkey:CustomerID" json:"customer"`
}
