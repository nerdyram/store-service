package model

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Session is responsible for holding a session info of an account.
// Security operations will be operate on this model
type Session struct {
	ID        uint      `gorm:"primary_key" json:"id"`
	AccountID uint      `json:"-"`
	Token     uuid.UUID `gorm:"type:uuid" json:"token"`
	CreatedAt time.Time `json:"createdAt"`
	Status    string    `json:"status"`
}
