package model

// Account will hold information about an account.
// Account can have a role that are Admin, Courier and Customer
type Account struct {
	ID                  uint    `gorm:"primary_key" json:"id"`
	Username            string  `json:"username"`
	Password            string  `json:"-"`
	PersonOrCompanyName string  `json:"name"`
	Address             string  `json:"address"`
	PhoneNumber         string  `json:"phoneNumber"`
	Role                string  `json:"role"`
	Status              string  `json:"status"`
	Session             Session `gorm:"foreignkey:AccountID" json:"session"`
}
