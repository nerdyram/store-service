package model

// Unit will hold information about a unit of item.
type Unit struct {
	ID     uint   `gorm:"primary_key" json:"id"`
	Name   string `json:"name"`
	Status string `json:"status"`
}
