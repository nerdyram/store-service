package model

// Item will hold information about an item.
type Item struct {
	ID             uint   `gorm:"primary_key" json:"id"`
	Name           string `json:"name"`
	RequiresRecipe bool   `json:"requiresRecipe"`
	Units          []Unit `gorm:"many2many:item_units" json:"units"`
	Status         string `json:"status"`
}
