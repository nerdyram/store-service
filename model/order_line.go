package model

// OrderLine will hold information about an order line that is related to an order.
type OrderLine struct {
	ID       uint    `gorm:"primary_key" json:"id"`
	OrderID  uint    `json:"-"`
	UnitID   uint    `json:"-"`
	Unit     Unit    `gorm:"foreignkey:UnitID" json:"unit"`
	ItemID   uint    `json:"-"`
	Item     Item    `gorm:"foreignkey:ItemID;association_autoupdate:false;association_autocreate:false" json:"item"`
	Quantity float32 `json:"quantity"`
	Status   string  `json:"status"`
}
