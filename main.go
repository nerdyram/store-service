package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"store-service/controller"
	"store-service/util"

	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func main() {
	appConfig := readConfiguration()
	util.InitDatabase()

	//prepare routes
	serviceRoutes := make(map[string]util.MuxRoute)
	serviceRoutes = mergeRoutes(serviceRoutes, controller.InitItemServiceRoutes())
	serviceRoutes = mergeRoutes(serviceRoutes, controller.InitUnitServiceRoutes())
	serviceRoutes = mergeRoutes(serviceRoutes, controller.InitOrderServiceRoutes())
	serviceRoutes = mergeRoutes(serviceRoutes, controller.InitAccountServiceRoutes())

	handler := util.InitMux(serviceRoutes)
	log.Println("Store Service API will accept connections at port: " + appConfig.Port)
	err := http.ListenAndServe(":"+string(appConfig.Port), handler)
	if err != nil {
		log.Fatalln("Cannot start service: " + err.Error())
		os.Exit(500)
	}
}

// Config holds the application configuration
type Config struct {
	Port string `json:"port"`
}

func readConfiguration() Config {
	data, readConfigErr := ioutil.ReadFile("config.json")
	if readConfigErr != nil {
		log.Fatalln("Configuration file is not available!")
		os.Exit(501)
	}

	configJSON := Config{}

	readConfigErr = json.Unmarshal(data, &configJSON)

	if readConfigErr != nil {
		log.Fatalln("Configuration file is invalid!")
		os.Exit(502)
	}

	return configJSON
}

func mergeRoutes(firstRoutes map[string]util.MuxRoute, secondRoutes map[string]util.MuxRoute) map[string]util.MuxRoute {
	for key, val := range secondRoutes {
		_, keyExists := firstRoutes[key]
		if keyExists {
			log.Fatalln("Please recheck all of your service routes. There are duplicate ones!")
			os.Exit(510)
		}
		firstRoutes[key] = val
	}
	return firstRoutes
}
