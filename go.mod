module store-service

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.14
	github.com/rs/cors v1.7.0
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	golang.org/x/tools v0.0.0-20200707200213-416e8f4faf8a // indirect
)
